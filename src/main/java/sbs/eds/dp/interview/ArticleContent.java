package sbs.eds.dp.interview;

public class ArticleContent {

    //the id
    public int id;
    //the headline
    public String title;
    /**
     * The body
     */
    public String body_text;
    /**
     * The abstract
     */
    public String abstracttext;

    /**
     * Any tags
     */
    public String[] tags;

    /**
     * The author id's
     */
    String[] authors;

    /**
     * Get the id
     * @return
     */
    public int getId() {
        return id;
    }

    public Editor newEditor(String name){
        Editor e = new Editor();
        e.name = name;
        return e;
    }

    public class Editor {

        //the name
        String name;
    }

}
