package sbs.eds.dp.interview;

public class PersonContent {

    public String id;
    public String name;
    public String name_f;
    public String name_l;
    public String[] roles;

    /**
     * The id
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * If the person is an editor
     *
     * @return
     */
    public boolean isEditor() {
        boolean isEditor = false;
        for (String role : roles) {
            if (role == "editor") {
                isEditor = true;
            }
        }
        return isEditor;
    }

    public boolean isSenior() {
        boolean senior = false;
        for (String role : roles) {
            if (role == "senior") {
                senior = true;
            }
        }
        return senior;
    }

    public boolean isJunior() {
        //        boolean senior = false;
        //        for (String role : roles) {
        //            if (role == "senior") {
        //                senior = true;
        //            }
        //        }
        //
        return false;
    }
}
