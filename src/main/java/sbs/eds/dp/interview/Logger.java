package sbs.eds.dp.interview;

public class Logger {

    public static final int LEV_DEBUG = 0;
    public static final int LEV_INFO = 1;
    public static final int LEV_WARN = 2;
    public static final int LEV_ERROR = 3;

    private final String logName;
    private final int level;

    public Logger(Class<?> classToLog) {
        this(classToLog.getName(), LEV_DEBUG);
    }

    public Logger(String logName, int level) {
        this.logName = logName;
        this.level = level;
    }

    public void debug(String msg) {
        log(LEV_DEBUG, "DEBUG", msg);
    }

    public void debug(String msg, Object... params) {
        log(LEV_DEBUG, "DEBUG", msg, params);
    }

    public void info(String msg) {
        log(LEV_INFO, "INFO", msg);
    }

    public void info(String msg, Object... params) {
        log(LEV_INFO, "INFO", msg, params);
    }

    public void warn(String msg) {
        log(LEV_WARN, "WARN", msg);
    }

    public void warn(String msg, Object... params) {
        log(LEV_WARN, "WARN", msg, params);
    }

    public void error(String msg) {
        log(LEV_ERROR, "ERROR", msg);
    }

    public void error(String msg, Object... params) {
        log(LEV_ERROR, "ERROR", msg, params);
    }

    public void log(int level, String prefix, String msg) {
        if (level > this.level) {
            System.out.println("[" + prefix + "]" + logName + " " + msg);
        }
    }

    public void log(int level, String prefix, String msg, Object... params) {
        if (level > this.level) {
            System.out.println(String.format("[" + prefix + "] " + logName + " " + msg, params));
        }
    }

}
