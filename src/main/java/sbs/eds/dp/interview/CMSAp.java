package sbs.eds.dp.interview;

import java.util.ArrayList;

public interface CMSAp {

    ArticleContent getArticleById(String id);

    ArrayList<ArticleContent> getArticleByTitle(String titleMatch);

    ArrayList<ArticleContent> getAllArticles();

    PersonContent getPersonById(String id);

    PersonContent getPersonByName(String id);
}
