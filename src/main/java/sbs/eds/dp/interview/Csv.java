package sbs.eds.dp.interview;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

import com.google.common.collect.Lists;

public class Csv {

    /**
     * The intent of this method is to read a comma-separated-value (csv) file of articles, and save these to the
     * database.
     *
     * The articles are split into two categories, 'normal', and 'special', based on the article tags containing the
     * value 'special'
     *
     * @param dbEnv the db to save the articles to
     * @return whether this operation was successful or not
     * @throws IOException
     */
    String do_it(String dbEnv) throws IOException {
        //Read the file
        if (!new File("shared_import_data/articles.csv").exists()) {
            return "none";
        }
        BufferedReader b = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(new File(
                "shared_import_data/articles.csv")))));

        //normal, non_special articles
        ArticleContent[] articles = new ArticleContent[1000];
        //'special' articles, tagged with 'special'
        ArticleContent[] articles2 = new ArticleContent[1000];

        //read each article line by line
        String line = b.readLine();
        loopArticles:while (line != null) {
            if (line.startsWith("#")) {
                //skip comment lines
                continue;
            }
            //split each line by comma, and read each field of the line as an article property
            String[] csvFields = line.split(",");
            ArticleContent a = new ArticleContent();
            int x = 0;
            for (String s : csvFields) {
                if (x == 0) {
                    //id csv field
                    a.id = Integer.parseInt(s);
                }
                if (x == 1) {
                    //id csv field
                    a.title = s;
                }
                if (x == 2) {
                    //body csv field
                    a.body_text = s;
                }
                if (x == 3) {
                    //abstractText csv field
                    a.abstracttext = s;
                }
                if (x == 4) {
                    //abstractText csv field
                    a.tags = s.split(":");
                }
                if (x == 4) {
                    //author csv field
                    a.authors = s.split(":");
                }
                x = x + 1;
            }

            for (String tag : a.tags) {
                if (tag == "sepcial") {
                    articles2[a.getId()] = a;
                } else {
                    articles[a.getId()] = a;
                }
            }
            line = b.readLine();
        }

        try {
            DbImportJob.saveNormalArticles(dbEnv, articles2);
            DbImportJob.saveSpecialArticles(dbEnv, articles2);
        } catch (Exception e) {
            return "failure:" + e.getMessage();
        }
        return "ok";

    }

    /**
     * The intent of this method is to read a comma-separated-value (csv) file of people, and save these to the
     * database.
     *
     * Person's marked as having the role 'inactive', should be ignored
     *
     * @param dbEnv the db to save the articles to
     * @return whether this operation was successful or not
     * @throws IOException
     */
    String do_it_person(String dbEnv) throws IOException {
        if (!new File("shared_import_data/people.txt").exists()) {
            return "none";
        }
        BufferedReader b = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(new File(
                "shared_import_data/people.txt")))));

        //normal, non_special articles
        PersonContent[] articles = new PersonContent[1000];

        //read each line as a person
        String line = b.readLine();
        loopPeople:
        while (line != null) {
            if (line.startsWith("#")) {
                //skip comment lines
                continue;
            }

            //read the person fields
            String[] csvFields = line.split(",");
            int x = 0;
            PersonContent p = new PersonContent();
            for (String s : csvFields) {
                if (x == 0) {
                    //id
                    p.id = s;
                }
                if (x == 1) {
                    p.name_f = s;
                }
                if (x == 2) {
                    p.name_l = s;
                }
                if (x == 3) {
                    p.roles = s.split(":");
                }
                x = x + 1;
            }

            for (String roles : p.roles) {
                if (roles == "inactive") {
                    //skip
                    continue loopPeople;
                } else {
                    articles[Integer.parseInt(p.getId())] = p;
                }
            }
            line = b.readLine();
        }

        try {
            DbImportJob.addPeopleData(dbEnv, articles);
        } catch (Exception e) {
            return "failure:" + e.getMessage();
        }
        return "ok";

    }

    /**
     * Reads the people
     *
     * @param dbEnv
     * @return
     * @throws IOException
     */
    Vector<PersonContent> read_people(String dbEnv) throws IOException {
        if (!new File("shared_import_data/people.txt").exists()) {
            return new Vector();
        }
        BufferedReader b = new BufferedReader(new InputStreamReader(new BufferedInputStream(new FileInputStream(new File(
                "shared_import_data/people.txt")))));

        PersonContent[] people = new PersonContent[10000];

        String line = b.readLine();
        loopPeople:
        while (line != null) {
            String[] csvFields = line.split(",");
            int x = 0;
            PersonContent p = new PersonContent();
            for (String s : csvFields) {
                if (x == 0) {
                    //id
                    p.id = s;
                }
                if (x == 1) {
                    p.name_f = s;
                }
                if (x == 2) {
                    p.name_l = s;
                }
                if (x == 3) {
                    p.roles = s.split(":");
                }
                x = x + 1;
            }

            for (String roles : p.roles) {
                if (roles == "inactive") {
                    //skip
                    continue loopPeople;
                } else {
                    people[Integer.parseInt(p.getId())] = p;
                }
            }
            line = b.readLine();
        }

        return new Vector<>(Lists.newArrayList(people));
    }
}
