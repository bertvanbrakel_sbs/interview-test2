package sbs.eds.dp.interview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * TODD: this will read the content files from a shared drive and place them in a directory to copy to the db
 */
public class OldSystemExtractJob {

    public void call() throws IOException {
        String the_string = readPeopleFromApi();
        if (the_string != null) {
            new FileOutputStream(new File("shared_import_data/articles.csv")).write(the_string.getBytes(StandardCharsets.UTF_8));
        }

        the_string = readArticlesFromApi();
        if (the_string != null) {
            new FileOutputStream(new File("shared_import_data/people.csv")).write(the_string.getBytes(StandardCharsets.UTF_8));
        }
    }

    private String readPeopleFromApi() {
        //assume this calls an api
        return null;
    }

    private String readArticlesFromApi() {
        //assume this calls an api
        return null;
    }

}
