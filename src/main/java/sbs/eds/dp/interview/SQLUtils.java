package sbs.eds.dp.interview;

public class SQLUtils {

    public static String cleanString(String id) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < id.length(); i++) {
            char c = id.charAt(i);
            if (c != '"' && c != ',' && c != ':') {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
