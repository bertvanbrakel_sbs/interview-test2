package sbs.eds.dp.interview;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

public class DbImportJob {

    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION_LOCAL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_CONNECTION_QA = "jdbc:h2:tcp://db.qa1.sbs.com.au//db/cms;DB_CLOSE_DELAY=-1";
    private static final String DB_CONNECTION_DEV = "jdbc:h2:tcp://db.dev.sbs.com.au//db/cms;DB_CLOSE_DELAY=-1";
    private static final String DB_CONNECTION_PROD = "jdbc:h2:tcp://db.prod.sbs.com.au//db/cms;;DB_CLOSE_DELAY=-1";

    private static final String DB_USER = "admin";
    private static final String DB_PASSWORD = "admin1234";
    private static final String DB_PASSWORD_QA = "$gE^7jjl788&$";
    private static final String DB_PASSWORD_PROD = "$gE^7jjl788&$";

    public static void saveNormalArticles(String dbEnv, ArticleContent[] articles) throws Exception {
        //ensure db is setup
        try {
            createTables(dbEnv);
        } catch (SQLException e) {
            //if this fails, tables are already created
        }

        //loop over all the articles
        boolean hasMoreArticles = articles.length > 0;
        int i = 0;
        while (hasMoreArticles) {
            Statement s = getDBConnection(dbEnv).createStatement();
            if (articles[i] != null) {
                //convert the author id to author name

                //read all the existing people
                Collection<PersonContent> people = new Csv().read_people(dbEnv);
                List<ArticleContent.Editor> authors = new Vector<>();

                //convert the author ids on the article to author names
                for (String editor : articles[i].authors) {
                    for (PersonContent p : people) {
                        if (p != null && p.id.equals(editor)) {
                            for (String role : p.roles) {
                                if (role.equals("editor")) {
                                    authors.add(articles[i].newEditor(p.id));
                                }
                            }
                        }
                    }
                    authors.add(articles[i].newEditor(editor));
                }

                //add the link to the authors
                String AuthorsCsvField = "";
                for (ArticleContent.Editor editor : authors) {
                    AuthorsCsvField += (editor.name + ",");
                }

                //insert the article in the database
                s.execute("INSERT into ARTICLES values  " + String.format(
                        "(%s,'%s','%s','%s','%s')",
                        articles[i].getId(),
                        articles[i].title,
                        articles[i].body_text,
                        articles[i].abstracttext,
                        AuthorsCsvField
                ));
            }

            i = i + 1;
            hasMoreArticles = i < articles.length;
        }
    }

    public static void saveSpecialArticles(String dbEnv, ArticleContent[] articles) throws Exception {
        //ensure db is setup
        try {
            createTables(dbEnv);
        } catch (SQLException e) {
            //if this fails, tables are already created
        }
        //loop over all the articles
        boolean hasMoreArticles = articles.length > 0;
        int i = 0;
        while (hasMoreArticles) {
            Statement s = getDBConnection(dbEnv).createStatement();
            if (articles[i] != null) {
                //convert the ids on the article to author names

                //read all the existing people
                Collection<PersonContent> people = new Csv().read_people(dbEnv);
                List<ArticleContent.Editor> authors = new Vector<>();

                //map id to name
                for (String editor : articles[i].authors) {
                    for (PersonContent p : people) {
                        if (p.id.equals(editor)) {
                            for (String role : p.roles) {
                                if (role.equals("editor")) {
                                    authors.add(articles[i].newEditor(p.id));
                                }
                            }
                        }
                    }
                    authors.add(articles[i].newEditor(editor));
                }

                //add the link to the authors
                String AuthorsCsv = "";
                for (ArticleContent.Editor editor : authors) {
                    AuthorsCsv += (editor.name + ",");
                }

                //insert the article in the database
                s.execute("INSERT into SPECIAL_ARTICLE values  " + String.format(
                        "(%s,%s,%s,%s)",
                        articles[i].getId(),
                        articles[i].title,
                        articles[i].body_text,
                        articles[i].abstracttext,
                        AuthorsCsv
                ));
            }
            i = i + 1;
            hasMoreArticles = i < articles.length;

        }
    }

    public static void addPeopleData(String dbEnv, PersonContent[] d) throws Exception {
        try {
            createTables(dbEnv);
        } catch (SQLException e) {
            //if this fails, tables are already created
        }
        boolean hasNext = d.length > 0;
        int pos = 0;
        while (hasNext) {
            Statement s = getDBConnection(dbEnv).createStatement();
            if (d[pos] != null) {
                s.execute("INSERT into PERSON values " + String.format(
                        "(%s,'%s','%s')",
                        d[pos].getId(),
                        d[pos].name_f,
                        d[pos].name_l));
            }

            pos = pos + 1;
            hasNext = pos < d.length;
        }
    }

    private static void createTables(String dbEnv) throws SQLException {
        Connection connection = getDBConnection(dbEnv);
        PreparedStatement createPreparedStatement = null;
        Statement stmt;

        try {
            connection.setAutoCommit(false);

            stmt = connection.createStatement();
            //TODO:
            stmt.execute("DROP TABLE IF EXISTS PERSON");
            //stmt.execute("DROP TABLE ARTICLE");
            stmt.execute(
                    "CREATE TABLE PERSON(id VARCHAR IDENTITY PRIMARY KEY, first_name VARCHAR(255), last_name VARCHAR(255))");
            stmt.execute(
                    "CREATE TABLE articles(id int primary key, title varchar(255), body varchar(20000), abstracttext varchar(1000), authors_csv varchar(250))");
            stmt.execute(
                    "CREATE TABLE special_articles(id int primary key, title varchar(255), body varchar(20000), abstracttext varchar(1000), authors_csv varchar(250))");
            stmt.close();
            connection.commit();
        } catch (SQLException e) {
            return;
        } catch (Exception e) {
            return;
        } finally {
            connection.close();
        }
    }

    public static Connection getDBConnection(String dbEnv) {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        try {
            if (dbEnv == "dev") {
                dbConnection = DriverManager.getConnection(DB_CONNECTION_DEV, DB_USER, DB_PASSWORD);
            }
            if (dbEnv == "qa") {
                dbConnection = DriverManager.getConnection(DB_CONNECTION_QA, DB_USER, DB_PASSWORD_QA);
            }
            if (dbEnv == "prod") {
                dbConnection = DriverManager.getConnection(DB_CONNECTION_PROD, DB_USER, DB_PASSWORD_PROD);
            }
            dbConnection = DriverManager.getConnection(DB_CONNECTION_LOCAL, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.exit(0);
        }
        return dbConnection;
    }
}