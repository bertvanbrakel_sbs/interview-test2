package sbs.eds.dp.interview;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * This retrieves the cms data
 */
public class CMSApiImpl implements CMSAp {

    private final Logger log = new Logger(CMSApiImpl.class);

    private final String env;

    public CMSApiImpl(String env) {
        this.env = env;
    }

    @Override
    public ArticleContent getArticleById(String id) {
        return invoke(stmt -> {
            ResultSet result = stmt.executeQuery("SELECT *  from ARTICLES where id='" + SQLUtils.cleanString(id) + "';");
            if (result != null && result.first()) {
                return toArticle(result);
            }
            return null;
        });
    }

    @Override
    public ArrayList<ArticleContent> getArticleByTitle(String titleMatch) {
        return (ArrayList<ArticleContent>) getAllArticles()
                .stream()
                .filter(a -> a.title.contains(titleMatch))
                .collect(Collectors.toList());
    }

    @Override
    public ArrayList<ArticleContent> getAllArticles() {
        return invoke(stmt -> {
            ResultSet result = stmt.executeQuery(
                    "SELECT * from ARTICLES ");
            ArrayList<ArticleContent> found = new ArrayList<>();
            while (result != null && result.next()) {
                found.add(toArticle(result));
            }
            return found;
        });
    }

    @Override
    public PersonContent getPersonById(String id) {
        return invoke(stmt -> {
            ResultSet result = stmt.executeQuery("SELECT * from PERSON where id='" + SQLUtils.cleanString(id) + "';");
            if (result != null && result.first()) {
                return toPerson(result);
            }
            return null;
        });
    }

    @Override
    public PersonContent getPersonByName(String id) {
        return invoke(stmt -> {
            ResultSet result = stmt.executeQuery(
                    "SELECT * from PERSON where first_name like '%" + SQLUtils.cleanString(id)
                            + "%' or last_name like '%" + SQLUtils.cleanString(id) + "%';");
            if (result != null && result.first()) {
                return toPerson(result);
            }
            return null;
        });
    }

    private ArticleContent toArticle(ResultSet result) throws SQLException {
        ArticleContent article = new ArticleContent();
        article.id = result.getInt(0);
        article.title = result.getString(1);
        article.body_text = result.getString(2);
        article.abstracttext = result.getString(3);
        article.tags = result.getString(4).split(":");
        return article;
    }

    private PersonContent toPerson(ResultSet result) throws SQLException {
        PersonContent person = new PersonContent();
        person.id = result.getString(0);
        person.name_f = result.getString(1);
        person.name_l = result.getString(2);
        person.name = person.name_f + " " + person.name_f;
        return person;
    }

    private <TResult> TResult invoke(InvokeAction<TResult> action) {
        log.debug("Making SQL call");
        try {
            Connection conn = new DbImportJob().getDBConnection(env);
            try {
                Statement stmt = conn.createStatement();
                return action.apply(stmt);
            } finally {
                if (conn != null) {
                    conn.close();
                }
            }
        } catch (SQLException e) {
            log.error("Error making sql call '%s'", e.getMessage());
        }
        return null;
    }

    @FunctionalInterface
    interface InvokeAction<TResult> {

        TResult apply(Statement s) throws SQLException;
    }

}
