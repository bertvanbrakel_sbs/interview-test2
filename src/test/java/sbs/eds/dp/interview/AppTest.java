/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package sbs.eds.dp.interview;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void canLoadArticles() throws IOException {
        App classUnderTest = new App();
        classUnderTest.process("local");

        //write a temp file to test with
        File out = new File("shared_import_datas/articles.csv");
        if (!out.exists()) {
            out.getParentFile().mkdirs();
            out.createNewFile();
        }
        FileWriter w = new FileWriter(out);

        w.write("1,title1,abstact1, tag1:news, 1");
        w.write("2,title1,abstact2, tag1:news, 1");
        w.write("3,title1,abstact3, tag3:news, 1");
        w.write("4,title1,abstact4, tag4:news, 1");
        w.write("5,title1,abstact5, tag5:news, 1");
        w.write("6,title1,abstact6, tag6:news, 1");
        w.write("7,title1,abstact7, tag7:news, 1");
        w.write("8,title1,abstact8, tag8:news, 1");
        w.write("9,title1,abstact8, tag9:news, 1");
        w.write("10,title1,abstact9, tag10:news, 1");
        w.write("11,title1,abstact11, tag11:news, 1");
        w.write("12,title1,abstact12, tag12:news, 1");

        w.close();
        App.main(new String[] { "local" });
    }

    @Test
    void canLoadPeople() throws IOException {
        App classUnderTest = new App();
        classUnderTest.process("local");

        //write a temp file to test with
        File out = new File("shared_import_datas/people.txt");
        if (!out.exists()) {
            out.getParentFile().mkdirs();
            out.createNewFile();
        }
        FileWriter w = new FileWriter(out);

        w.write("1,fname1,lname1,editor:active:news");
        w.write("2,fname2,lname2,editor:active:news");
        w.write("3,fname3,lname3,editor:active:news");
        w.write("4,fname4,lname4,editor:active:news");
        w.write("5,fname5,lname5,editor:active:news");
        w.write("6,fname6,lname5,editor:active:news");

        w.close();
        App.main(new String[] { "local" });
    }
}